import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class CompanyDetailsComponent implements OnInit {

  companiesDetails: any = [
    { id: 1, name: "ABC Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 1 },    
    { id: 2, name: "ABC2 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 2 },
    { id: 3, name: "ABC3 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 3 },
    { id: 4, name: "ABC4 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 4 },
    { id: 5, name: "ABC5 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 5 },
    { id: 6, name: "ABC6 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 6 },
    { id: 7, name: "ABC7 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 7 },
    { id: 8, name: "ABC8 Pte Ltd", tel: 32432423, fax: 32432423, address: "dhddhdhd", rcbNo: 8 }
  ]
  
  private selectedId: number;
  companyInfo = null;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    console.log("company details ...");
    this.route.params.subscribe(params => {
      this.selectedId = +params['id']; 
      console.log(this.selectedId);
      this.companiesDetails.find((element)=>{
        console.log(element.id);
        if(element.id == this.selectedId){
          this.companyInfo = element;
          console.log(this.companyInfo);
        }
      });
    });
  }

  goBack(){
    this.router.navigate(['/Company']);
  }

}
