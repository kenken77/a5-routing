import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  companies: any = [
    { id: 1, name: "ABC Pte Ltd" },    
    { id: 2, name: "ABC2 Pte Ltd" },
    { id: 3, name: "ABC3 Pte Ltd" },
    { id: 4, name: "ABC4 Pte Ltd" },
    { id: 5, name: "ABC5 Pte Ltd" },
    { id: 6, name: "ABC6 Pte Ltd" },
    { id: 7, name: "ABC7 Pte Ltd" },
    { id: 8, name: "ABC8 Pte Ltd" }
  ]

  constructor(private router: Router) { }

  ngOnInit() {
    
  }

}
