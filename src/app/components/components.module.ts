import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { CompanyComponent } from '../components/company/company.component';
import { CustomersComponent } from '../components/customers/customers.component';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';
import { CompanyDetailsComponent } from './company/details/details.component';

const appRoutes: Routes = [
    {
        path: 'Home',
        component: HomeComponent,
    },
    {
        path: 'Company',
        component: CompanyComponent,
    },
    {
        path: 'Customers',
        component: CustomersComponent,
    },
    {
        path: 'companyDetails/:id',
        component: CompanyDetailsComponent,
    },

    { path: '',   redirectTo: '/Home', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent }
];


@NgModule({
    declarations: [
        HomeComponent,
        CompanyComponent,
        CustomersComponent,
        PageNotFoundComponent,
        CompanyDetailsComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only
            }
        )
    ],
    exports: [
        HomeComponent,
        CompanyComponent,
        CustomersComponent,
        PageNotFoundComponent,
        CompanyDetailsComponent,
        RouterModule
    ],
    providers: [
    ]
  })
  export class ComponentsModule { }